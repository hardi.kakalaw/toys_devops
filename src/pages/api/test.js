const executeQuery = require("../../lib/db");

export const yourAPI = async (req, res) => {
  const query = "SELECT * FROM `users`";
  const result = await executeQuery({ query });
  // ...rest of the code
};
