import { useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import styles from "../styles/loginPopup.module.css";
import Signup from "./signup";
import Image from "next/image";

const PORT = 3000;
const url = `http://localhost:${PORT}/api/login`;

export default function LoginPopup(props) {
  const { onClose, switchToSignup } = props;
  const router = useRouter();
  const href = "/recentproducts";
  // const href = "/TESTprofile";

  const [userData, setUserData] = useState({});
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  function handleLogin(e) {
    e.preventDefault();
    const userClient = {
      email: email,
      password: password,
    };
    const userJSON = JSON.stringify(userClient);
    console.log("USER JSON --- ", userJSON);
    // login
    fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: userJSON,
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.token) {
          setUserData(data);
          router.push({
            pathname: href,
            query: data,
          });
          console.log("Daaataa", data);
        } else {
          console.log("error-message", data.message);
          setErrorMessage(data.message);
        }
      });
  }

  return (
    <>
      <div onClick={onClose} className={styles.overlay}></div>

      <div className={styles.centerContainer}>
        <div className={styles.container}>
          <div className={styles.imageContainer}>
            <Image
              src="/NavbarLogo.png"
              alt="Logo image"
              width={100}
              height={100}
            />
          </div>
          <button className={styles.closeButton} onClick={onClose}>
            X
          </button>

          <div className={styles.form} action="">
            <div className={styles.login}>
              {/* inputs */}
              {errorMessage !== "" && (
                <p className={styles.error} onClick={() => setErrorMessage("")}>
                  {errorMessage}
                </p>
              )}
              <p className={styles.text}>Login to your account</p>
              <input
                className={styles.input}
                type="text"
                name="username"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <br />
              <input
                className={styles.input}
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <br />
            </div>

            <div className={styles.buttons}>
              {/* buttons */}
              <button
                className={styles.buttonSignIn}
                onClick={(e) => handleLogin(e)}
              >
                Sign in
              </button>
              <p className={styles.textOr}>Don&apos;t have an account?</p>
              <button
                onClick={() => switchToSignup()}
                className={styles.buttonSignUp}
              >
                Sign up
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
