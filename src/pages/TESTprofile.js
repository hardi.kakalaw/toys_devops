import Link from "next/link";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";

const url = "/api/auth";

export default function TESTProfile() {
  const router = useRouter();
  const { query } = router;
  console.log("query ::: ", query);

  const [auth, setAuth] = useState(false);

  useEffect(() => {
    const authorizeToken = async () => {
      try {
        const response = await fetch(url, {
          method: "GET",
          headers: { Authorization: `Bearer ${query.token}` },
        });

        if (response.ok) {
          const data = await response.json();
          if (data.message) {
            setAuth(true);
            return;
          }
        }
        router.push("/login");
      } catch (error) {
        console.error("An error occurred during authorization:", error);
        router.push("/login");
      }
    };

    if (query.token) {
      authorizeToken();
    } else {
      router.push("/login");
    }
  }, [query.token, router]);

  return (
    <div>
      <h1>hello TEST PROFILE: {query.username}</h1>
      <h3>token: {query.token}</h3>
      <h3>username: {query.username}</h3>
      <h3>
        isLoggedIn: {query.isLoggedIn}, is authed: {auth.toString()}
      </h3>
      <button>
        <Link href="/login">Login</Link>
      </button>
    </div>
  );
}
