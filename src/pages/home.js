import React, { useEffect, useState } from "react";
import { AiOutlineSearch } from "react-icons/ai";
import { Lilita_One, Montserrat } from "next/font/google";
import FirstCard from "./components/homeCards/FirstCard";
import styles from "./home.module.css";
import BrowseByCategory from "./browsebycategory";

const lilitaOne = Lilita_One({ weight: "400", subsets: ["latin"] });
const montserrat = Montserrat({ weight: "200", subsets: ["latin"] });

export default function HomeTest() {
  const [products, setProducts] = useState([]);

  async function fetchProducts() {
    try {
      const response = await fetch("http://localhost:3000/api/products", {
        method: "GET",
        mode: "cors",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      const jsonData = await response.json();
      setProducts(jsonData.products);
    } catch (error) {
      console.error("Error fetching products:", error);
    }
  }

  useEffect(() => {
    fetchProducts();
  }, []);

  console.log(products);

  return (
    <div>
      <div className={styles.box}>
        <h3 className={`${styles.welcometext} ${montserrat.className}`}>
          Welcome ... to Rejoi!
        </h3>
        <div>
          <div>
            <AiOutlineSearch
              className={styles.iconsearch}
              color="#5f3f3f"
              size={23}
            />
            <div>
              <input
                type="search"
                placeholder="What are you looking for today?"
                className={`${styles.searchbar} ${montserrat.className}`}
              />
            </div>
          </div>
        </div>
        <h1 className={`${styles.text} ${lilitaOne.className}`}>
          Most recently published donations
        </h1>
      </div>
      <div className={styles.importing}>
        {products.slice(0, 6).map((product) => (
          <div key={product.id}>
            <FirstCard product={product} />
          </div>
        ))}
      </div>
      <div className={styles.bear}></div>
      <BrowseByCategory />
    </div>
  );
}
