import Image from "next/image";
import styles from "./homeCard.module.css";

const FirstCard = ({ product }) => {
  return (
    <div className={styles.imgContainer}>
      <Image
        src={product.url}
        alt="robot"
        className={styles.img}
        width={300}
        height={300}
      />
      <div className={styles.cardtext}>{product.product_title}</div>
      <button className={styles.buttonstyle}>View</button>
    </div>
  );
};

export default FirstCard;
