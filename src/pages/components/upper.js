import { useState } from "react";
import Link from "next/link";
import { AiOutlineSearch } from "react-icons/ai";
import Image from "next/image";
import styles from "./nbar.module.css";

const Upper = () => {
  const [open, setOpen] = useState(true);

  return (
    <div className={styles.test}>
      <div className={styles.description}>
        <div className={styles.logoupper}>
          <Image src="/NavbarLogo.png" alt="Logo" width={200} height={50} />
        </div>
        <div>
          <div>
            <AiOutlineSearch
              className={styles.iconsearch}
              color="#5f3f3f"
              size={23}
            />
            <div>
              <input
                type="Search"
                placeholder="What are you looking for today?"
                className={styles.searchbar}
              />
            </div>
          </div>
        </div>

        <ul className={styles.ulLinks}>
          <li>
            <Link href="/home" className={styles.link}>
              Home
            </Link>
          </li>
          <li>
            <Link href="/about" className={styles.link}>
              About
            </Link>
          </li>
          <li>
            <Link href="/category" className={styles.link}>
              Categories
            </Link>
          </li>
          <li>
            <Link href="/donations" className={styles.link}>
              Donations
            </Link>
          </li>
          <li>
            <Link href="/inbox" className={styles.link}>
              Inbox
            </Link>
          </li>
          <li>
            <Link href="/settings" className={styles.link}>
              Settings
            </Link>
          </li>
        </ul>
        <div className={styles.hamburgerContainer}>
          <label className={styles.hamburgermenu}>
            <input type="checkbox" />
          </label>
          <aside className={styles.sidebar}>
            <ul>
              <li className={styles.sidbarUsername}>Username</li>
              <li className={styles.sidbarstyle}>
                <Link href="/home" className={styles.linkSidebar}>
                  About
                </Link>
              </li>
              <li className={styles.sidbarstyle}>
                <Link href="/home" className={styles.linkSidebar}>
                  Categories
                </Link>
              </li>
              <li className={styles.sidbarstyle}>
                <Link href="/home" className={styles.linkSidebar}>
                  Donation
                </Link>
              </li>
              <li className={styles.sidbarstyle}>
                <Link href="/home" className={styles.linkSidebar}>
                  Inbox
                </Link>
              </li>
              <li className={styles.sidbarstyle}>
                <Link href="/home" className={styles.linkSidebar}>
                  Settings
                </Link>
              </li>
            </ul>
          </aside>
        </div>
      </div>
    </div>
  );
};

export default Upper;
