import { FaFacebook } from "react-icons/fa";
import { RiInstagramFill } from "react-icons/ri";
import styles from "./footercss.module.css";
import Link from "next/link";
import Image from "next/image";

const Down = () => {
  return (
    <div className={styles.decoration}>
      <Image
        src="/pagedecoration.png"
        alt="blue decoration"
        className={styles.wavypic}
        width={100} // Replace with the desired width in pixels
        height={100} // Replace with the desired height in pixels
      />
      <div className={styles.footerContainer}>
        <div className={styles.leftbox}>
          <Image
            src="/FooterLogo.png"
            alt="Logo"
            className={styles.logodown}
            width={100} // Replace with the desired width in pixels
            height={100} // Replace with the desired height in pixels
          />
          <div>
            <p className={styles.textleft}>
              By sharing your toys, you can help to create a sense of connection
              and goodwill among families. So why not join us in our mission to
              make the world a better place, one toy at a time?
            </p>
          </div>
        </div>
        <div className={styles.rightbox}>
          <div>
            <h2 className={styles.headerSecondUl}>Company</h2>
            <ul className={styles.firstUl}>
              <li className={styles.li}>
                <Link href="/about">
                  <a className={`${styles.link}`}>About us</a>
                </Link>
              </li>
              <li className={styles.li}>
                <Link href="/privacy-policy">
                  <a className={`${styles.link}`}>Privacy Policy</a>
                </Link>
              </li>
              <li className={styles.li}>
                <Link href="/terms-of-service">
                  <a className={`${styles.link}`}>Terms of Service</a>
                </Link>
              </li>
            </ul>
          </div>
          <div className={`${styles.ulRight}`}>
            <h2 className={styles.headerFirstUl}>Support</h2>
            <ul className={styles.secondUl}>
              <li className={styles.li}>
                <Link href="/faq">
                  <a className={`${styles.link}`}>FAQ</a>
                </Link>
              </li>
            </ul>
          </div>
          <div className={styles.iconsRight}>
            <Link href="/about">
              <FaFacebook size="2rem" className={styles.facebookicon} />
            </Link>
            <Link href="/about">
              <RiInstagramFill size="2rem" className={styles.insta} />
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Down;
