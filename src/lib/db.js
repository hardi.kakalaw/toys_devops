const mysql = require("serverless-mysql");

const db = mysql({
  config: {
    host: process.env.DB_HOST || "localhost",
    database: process.env.DB_DATABASE || "toys_project",
    user: process.env.DB_USERNAME || "root",
    password: process.env.DB_PASSWORD || "",
    // port: 3306, // default port == 3306
    // ssl: { rejectUnauthorized: true }, // Uncomment if using SSL
  },
});

async function executeQuery({ query, values }) {
  try {
    const results = await db.query(query, values);
    await db.end();
    return results;
  } catch (error) {
    console.log("ERROR!", error);
    return { error };
  }
}

module.exports = executeQuery;
