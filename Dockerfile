# Use a specific version of the Node.js base image
FROM node:14-alpine

# Set the working directory
WORKDIR /app

# Copy only the changed application files with .js and .json extensions to the working directory
COPY *.js *.json ./

# Install dependencies
RUN npm ci --production

# Specify the command to run your application
CMD ["npm", "start"]
