module.exports = {
  testMatch: ["<rootDir>/tests/**/*.test.js"],
  transform: {
    "^.+\\.js$": "babel-jest", // Add this line to configure Babel transform
  },
  testEnvironment: "node",
};
